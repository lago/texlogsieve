# Changelog

All notable changes to `texlogsieve` will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.0] - 2025-03-11

### Added

 - Refill the buffer more often

 - extraFilesHandler now works correctly in RAW mode

 - Greatly improved epilogueHandler; files loaded and other messages
   after the epilogue begins do not disappear anymore

### Fixed

 - Correctly handle "--no-blah" options even if "blah" is defined
   in the config file

 - Fix subtle bug: closeSquareBracketHandler:doit() did not return true

 - Fix bug #19 in version 1.5.0 that could cause the program to freeze

## [1.5.0] - 2025-03-03

### Added

 - Add .log extension if necessary

 - Always convert "CR LF" to "LF" when reading the .log and .fls files

 - When showing an UNKNOWN message, also show the previous one
   if it was suppressed, as they may be related

 - Improved and refactored handling of error messages

 - Allow for some messages that differ only in some details to be
   identified as the same message

 - Identify some engine-specific errors and warnings

 - Greatly improved shipoutFilesHandler (preventing weird errors)

 - When adding a file or shipout to the stack, remove dangling "DUMMY" entry

## [1.4.2] - 2024-11-22

### Fixed

 - Corrected "runsystem (blah)...executed." pattern 

 - Fixed bug that prevented some summaries from being printed

### Changed

 - More messages

 - Correctly handle "\*full hbox while \output is active"

### Added

 - Mention pulp log parsing program

 - Messages from "comment" package

## [1.4.1] - 2024-01-15

### Changed

 - Look 5 lines ahead instead of 3

 - Recognize more messages

### Added

 - Reduce priority of harmless font substitutions
   ("m" -> "regular", "b" -> "bold")

## [1.4.0] - 2023-11-30

### Added

 - Suggest fixes for some known warnings

 - More messages

 - Handle files opened during shipout: {blah.tex}, <blah.pdf> etc.

## [1.3.1] - 2022-09-05

### Fixed

 - Bug #6: Searching for additional config files in Windows would
   fail and cause the program to crash

## [1.3.0] - 2022-08-05

### Added

 - Indicate whether there were parse errors in the summary

 - --verbose option

### Changed

 - Sort line numbers in summary

 - "Parse error" messages include the input line number

 - Search for a config file in the user's home directory too

### Fixed

## [1.2.0] - 2022-07-21

### Added

 - More messages

 - If package refcheck is loaded, add the list of unused labels to
   the summary

### Changed

 - Missing citations and characters summaries now uses colored keys
   and have fewer line breaks

 - Improve latexmk example script to exit with an error in case
   LaTeX compilation fails

 - Summary messages that mention pages and files now use singular
   and plural

 - Summary messages now include line numbers (if available)

### Fixed

 - Missing citations were sometimes not recognized

## [1.1.3] - 2022-04-22

### Fixed

 - Be a little more careful with continuation lines in error messages
   so that we do not get confused by extra parens such as

   ```
   ...) some continuation error message...
   ```

## [1.1.2] - 2022-03-14

### Fixed

 - Bug (typo) unwrapping lines that start with "]"

## [1.1.1] - 2022-03-05

### Fixed

 - Make two variables local instead of global

## [1.1.0] - 2022-03-04

### Added

 - Print "No important messages to show" when nothing is printed

 - Print "There were errors during processing! Generated PDF is
   probably defective" if there were error messages

 - More messages are recognized

### Fixed

 - Fix bug with filename and URL on the same line

 - Do not lose messages if the file is truncated

## [1.0.0] - 2022-02-09

### Added

 - Options `--file-banner` and `--no-file-banner`

 - Options `--color` and `--no-color`

 - Better compatibility with MiKTeX for Windows:

   - Recognize pathnames with backslashes

   - Recognize relative paths that do not start with "./"

   - Recognize MiKTeX "package maintenance" messages (auto-download
     new packages)

 - Use the .fls file instead of kpse to identify filenames if it
   exists and seems to be up-to-date

 - More messages are recognized, including "fp" package

### Fixed

 - Missing characters were not included in the summary without
   `--be-redundant`

### Changed

 - Messages like "Package blah SEVERITY" are now set as "UNKNOWN"
   if SEVERITY is not recognized (they were set as "WARNING" before)

 - `--file-banner` is enabled by default

 - Changed the effect of filters on the summary

## [1.0.0-beta-3] - 2022-02-02

### Added

 - Detect "please rerun" messages and show in summary

 - Options `--ref-detail`, `--cite-detail`, and `--summary-detail`

### Fixed

 - Minor bugs and limitations

 - Correctly handle line wrapping with XeTeX (count utf8 characters
   instead of bytes)

### Changed

 - Invalid command-line options are detected and cause the program to abort

## [1.0.0-beta-2] - 2022-01-04

### Added

 - Options `--be-redundant` and `--box-detail`

 - Options `--set-to-level-[debug|info|warning|critical]`

 - Config file texlogsieverc is always read if it exists

### Fixed

 - Options `--add-[info|warning]-message` did not work

 - Some minor bugs are fixed

### Changed

 - Silenced messages are not excluded from summaries

 - Empty citation/label keys are substituted for "???"

 - A few more messages are recognized

## [1.0.0-beta-1] - 2021-12-16

Initial prerelease version
