# TODO

## Short-term improvements

 * It is probably a good idea to detect things like "\[", "\(", "\<"
   and not consider them to be file/shipout candidates

 * improve epilogueHandler, as there may be some relevant warnings
   after the epilogue begins. I have seen at least
   "pdfTeX warning (dest): name{BLAH} has been referenced but does not exist, replaced by a fixed one"

 * When we add "DUMMY" to the files or shipout stacks, we should eventually
   give up searching for the corresponding ")" or "]" character.

 * Even with --no-box-detail, we should indicate if there is a box with
   badness 10,000

 * Create tests

     - Synthetic unit tests: these should systematically test the code and the
       features it implements with small "fake" log files

     - Real-world files (for example, the docs for some packages or some papers
       from arXiv): these should help us detect important log messages and
       changes in messages whenever new versions of LaTeX and its packages are
       released

## Other improvements

 * Implement option `--collapse-lines` (show multiline messages as a single
   line)

 * Options --no-ref-detail and --no-cite-detail might show an aggregated
   list of pages and filenames besides the list itself

 * Improve the final summary format - we should look at other tools for
   inspiration

 * Filter the summary report too; look at `texloganalyser` for inspiration

 * Improve error handling if/where possible

 * Better aggregation in the summary:

     - `under/overfull boxes: p.5 (chap1.tex, L27); p.7 (chap2.tex, L37); p.19
       (chap2.tex, L176)`

     - `missing characters X, Y, Z in font blah`

 * Try to figure out a way to automatically extract possible messages from
   LaTeX packages

## Refactorings to consider:

 * Modify `heartbeat` to use `os.difftime()`

 * Improve Lines:

     - Eliminate `Lines.current` and instead always use `Lines.get(0)`,
       `Lines.get(1)` etc. We might store stuff in indexes 1, 2, 3 etc. and
       make something like `Lines:get(val) => return Lines[val +1]` (this is
       better than actually using index 0 because the lua standard library
       counts from one, so adhering to this convention makes using it easier)

     - Reimplement the logic using <https://www.lua.org/pil/11.4.html> for a
       small performance increase
