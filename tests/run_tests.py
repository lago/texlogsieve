#!/usr/bin/env python3

# Each test case provides an input message and the corresponding
# expected output at different filtering levels. Therefore, for
# each test case, we need to run texlogsieve multiple times (using
# different values for the "-l" command-line option). run_test()
# contains a loop that will iterate over all of these "sub-cases"
# and call run_subtest() for each one. run_subtest() actually
# calls texlogsieve.

import shutil
from pathlib import Path


def main():
    load_opening_closing() # define globals

    testCases = load_tests()

    count = 0
    failcount = 0
    rundir = Path('run')
    for testCase in testCases:
        count += 1
        rundir.mkdir()

        try:
            result = run_test(rundir, testCase)

            if result:
                print("OK -", testCase['name'])
            else:
                failcount += 1
                print("FAIL -", testCase['name'])
                if 'description' in testCase:
                    print(testCase['description'])
                    print()

        except subprocess.CalledProcessError:
            failcount += 1
            print("Test case failed to run:", testCase['name'])
        finally:
            shutil.rmtree(rundir)

    print()
    print("Tests failed/ran: ", failcount, "/", count, sep='')


from ruamel import yaml

def load_opening_closing():
    with open ('opening.yml', 'r') as f:
        global opening
        opening = yaml.load(f, Loader=yaml.Loader)

    with open ('closing.yml', 'r') as f:
        global closing
        closing = yaml.load(f, Loader=yaml.Loader)

def load_tests():
    testCases = []

    for filename in Path('.').glob('*.yml'):
        if filename.name == 'closing.yml' or filename.name == 'opening.yml':
            continue

        with open (filename, 'r') as f:
            tmp = yaml.load(f, Loader=yaml.Loader)
            testCases.extend(tmp)

    return testCases


# We use this to compare different severity levels numerically
from enum import Enum
class Level(Enum):
    debug = 0
    info = 1
    warning = 2
    critical = 3
    unknown = 4

def run_test(rundir, testCase):
    prepare_test_files(rundir, testCase)

    result = True # We start with no failures

    for nominalLevel in ['debug', 'info', 'warning', 'critical', 'unknown']:
        # The test may not define the output for level "debug" because
        # it is the same as the output for level "warning". Here, we
        # search for the next higher level and use it. We use an Enum
        # to make the comparisons between levels easier.
        effectiveLevel = Level[nominalLevel]
        while (effectiveLevel.value < Level['unknown'].value
                   and effectiveLevel.name not in testCase):

            i = effectiveLevel.value +1
            effectiveLevel = Level(i)

        if effectiveLevel.name not in testCase:
            # We did not find the next higher level, which means
            # we are testing a level that is higher than any level
            # defined in the test case (e.g., we are testing level
            # "warning" but the highest level defined in the test
            # case is "info"). When this happens, the message should
            # be suppressed (it is an "info" message).
            effectiveLevel = None
        else:
            effectiveLevel = effectiveLevel.name

        result = result and run_subtest(rundir, testCase, nominalLevel,
                                            effectiveLevel)
    return result

def prepare_test_files(rundir, testCase):
    rundir.joinpath('testcase.log').write_text(opening['message']
                                               + testCase['message']
                                               + closing['message'])

    # These always need to exist, as they are in the opening/closing
    rundir.joinpath('dummy.aux').touch()
    rundir.joinpath('dummy.tex').touch()

    if 'files' in testCase:
        for f in testCase['files']:
            rundir.joinpath(f).touch()

    if 'texlogsieverc' in testCase:
        rundir.joinpath('texlogsieverc').write_text(
                                        testCase['texlogsieverc'])


import subprocess

def run_subtest(rundir, testCase, nominalLevel, effectiveLevel):
    if effectiveLevel:
        expected = testCase[effectiveLevel]
    else:
        expected = ""

    if nominalLevel == 'debug':
        expected = opening['debug'] + expected

    shipoutcmd = '--no-shipouts'
    if 'shipouts' in testCase:
        shipoutcmd = '--shipouts'

        if nominalLevel == 'debug':
            expected = expected + closing['debugshipout']
        else:
            expected = expected + closing['unknownshipout']
    elif nominalLevel == 'debug':
        expected = expected + closing['debug']

    result = subprocess.run(["../../texlogsieve",
                             "-l", nominalLevel,
                             "--no-repetitions",
                             "--no-page-delay",
                             "--no-file-banner",
                             shipoutcmd,
                             "--no-summary",
                             "testcase.log"
                            ],
                            capture_output = True, check = True,
                            cwd = rundir, text = True)

    if expected == "":
        expected = "No important messages to show\n"

    return result.stdout == expected


main()
