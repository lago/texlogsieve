# Testing texlogsieve

## "Unit" tests

We do not have real unit tests. Instead, we execute `texlogsieve`
with some minimal, synthetic log files that exercise one specific
kind of message handler. To do that, we have a bunch of LaTeX-style
messages and the typical opening and closing lines that LaTeX
generates. We then combine the opening, some message, and the
closing to generate the synthetic logfile for testing.

For each message, we know the expected output in different modes,
i.e., with different filtering levels ("-l"). We run each test
case with different parameters and check if the result matches
what we expect.

The output is somewhat crude and more test cases need to be created,
but the basic testing framework is ok.

## Failing tests

The `failing` directory contains tests (in the spirit of the "unit"
tests) that demonstrate the cases in which we know `texlogsieve` fails
for one reason or another.

## Other tests

We should create other kinds of tests:

 * Combine multiple messages from the "unit" tests to generate a larger
   file with many messages in random order

 * Test with real-world files (maybe stuff from arxiv or the docs for
   some LaTeX packages)

 * Test the same input with different values for `max_print_line` and
   check whether the output of `-u` is the same

 * Test the various command-line options and combinations (that is a
   lot of stuff!)

 * Test whether multiple config files are read and processed correctly

 * Test line unwrapping with UTF-8 characters near the end of the line

 * Test lookahead

 * Test the various summaries

 * Test whether repetitions are eliminated correctly

 * Test unwrapping in the epilogue (it does not follow the "normal"
   line wrapping rules)

 * Test whether filenames are unwrapped correctly when there are two
   files with similar names

 * Test whether shipouts are unwrapped correctly

 * Test whether shipouts are counted correctly and whether the LaTeX
   counter for shipouts is extracted correctly
