# Test genericLatexHandler line unwrapping in different cases.

# "|2" means "indentation is two spaces", which allows us
# to have the first line start with any number of space
# characters that are not mistaken for indentation

-
  message: |2
    Package: dummy1 2021/10/10 v1.0 this is just an overly long description of a pa
    ckage
  info: |2
    Package: dummy1 2021/10/10 v1.0 this is just an overly long description of a package
  name: Simple wrapped "provides" line

-
  message: |2
    Package dummy1 Info: genericLatexHandler unwrapping - one line
    (dummy1)             Wrapped line start 123456789012345678901234567890123456789
    Wrapped line end
    (dummy1)             Non-wrapped line
  info: |2
    Package dummy1 Info: genericLatexHandler unwrapping - one line
    (dummy1)             Wrapped line start 123456789012345678901234567890123456789Wrapped line end
    (dummy1)             Non-wrapped line
  name: genericLatexHandler unwrapping - one line

-
  message: |2
    Package dummy2 Warning: genericLatexHandler unwrapping - many lines
    (dummy2)                Non-wrapped line
    (dummy2)                Wrapped line start 456789012345678901234567890123456789
    Wrapped line, continued 5678901234567890123456789012345678901234567890123456789
    Wrapped line, continued 5678901234567890123456789012345678901234567890123456789
    Wrapped line end
  warning: |2
    Package dummy2 Warning: genericLatexHandler unwrapping - many lines
    (dummy2)                Non-wrapped line
    (dummy2)                Wrapped line start 456789012345678901234567890123456789Wrapped line, continued 5678901234567890123456789012345678901234567890123456789Wrapped line, continued 5678901234567890123456789012345678901234567890123456789Wrapped line end
  name: genericLatexHandler unwrapping - many lines

-
  message: |2
    Package dummy3 Info: genericLatexHandler unwrapping - fake wrapped at end
    (dummy3)             This may fail if the following message is unknown - There
    (dummy3)             is another test case that explicitly fails in that case
    (dummy3)             Fake wrapped line 0123456789012345678901234567890123456789
  info: |2
    Package dummy3 Info: genericLatexHandler unwrapping - fake wrapped at end
    (dummy3)             This may fail if the following message is unknown - There
    (dummy3)             is another test case that explicitly fails in that case
    (dummy3)             Fake wrapped line 0123456789012345678901234567890123456789
  name: genericLatexHandler unwrapping - fake wrapped line at end
  description: >-
    The last line is not a wrapped line, but is 79 characters long,
    so it may be mistaken for a wrapped line if the following message
    is unknown, but should work ok otherwise.

-
  message: |2
    Package dummy4 Warning: genericLatexHandler unwrapping - fake wrapd in middle
    (dummy4)                Non-wrapped line
    (dummy4)                Fake wrapped line 3456789012345678901234567890123456789
    (dummy4)                Non-wrapped line
  warning: |2
    Package dummy4 Warning: genericLatexHandler unwrapping - fake wrapd in middle
    (dummy4)                Non-wrapped line
    (dummy4)                Fake wrapped line 3456789012345678901234567890123456789
    (dummy4)                Non-wrapped line
  name: genericLatexHandler unwrapping - fake wrapped line in the middle
  description: >-
    One of the lines is not a wrapped line, but is 79 characters long. It
    should not be mistaken for a wrapped line because the following line has
    a prefix, indicating it is just another ordinary line in the same message.

-
  message: |2
    Package dummy5 Info: genericLatexHandler unwrapping - wrap w/ parens 1
    (dummy5)             Non-wrapped line
    (dummy5)             Wrapped line start 123456789012345678901234567890123456789
    Wrapped line, continued 5678901234567890123456789012345678901234567890123 (6789
    ) Wrapped line end
  info: |2
    Package dummy5 Info: genericLatexHandler unwrapping - wrap w/ parens 1
    (dummy5)             Non-wrapped line
    (dummy5)             Wrapped line start 123456789012345678901234567890123456789Wrapped line, continued 5678901234567890123456789012345678901234567890123 (6789) Wrapped line end
  name: genericLatexHandler unwrapping - wrapped line with open/close parens, test 1
  description: >-
    The last line is wrapped and the continuation starts at the "close parens"
    character. While a known message at the beginning of a line (in this case,
    the close parens character) should normally prevent line unwrapping, we
    treat this case specially: since there is an unmatched "open parens"
    character in the preceding line, we just unwrap the line normally.

-
  message: |2
    Package dummy6 Warning: genericLatexHandler unwrapping - wrap w/ parens 2
    (dummy6)                Wrapped line start 456789012345678901234567890123456789
    Wrapped line, continued (6789) 234567890123456789012345678901234567890123 (6789
    ) Wrapped line end
    (dummy6)                Non-wrapped line
  warning: |2
    Package dummy6 Warning: genericLatexHandler unwrapping - wrap w/ parens 2
    (dummy6)                Wrapped line start 456789012345678901234567890123456789Wrapped line, continued (6789) 234567890123456789012345678901234567890123 (6789) Wrapped line end
    (dummy6)                Non-wrapped line
  name: genericLatexHandler unwrapping - wrapped line with open/close parens, test 2
  description: >-
    One of the lines is wrapped and the continuation starts at the "close
    parens" character. While a known message at the beginning of a line (in
    this case, the close parens character) should normally prevent line
    unwrapping, we treat this case specially: since there is an unmatched
    "open parens" character in the preceding line, we just unwrap the line
    normally.

-
  message: |2
    Package dummy7 Info: genericLatexHandler unwrapping - wrap w/ parens 3
    (dummy7)             Non-wrapped line
    (dummy7)             Wrapped line start 123456789012345678901234567890123 (6789
    Wrapped line, continued 56789) 23456789012345678 (1234) 78901234567890123 (6789
    ) Wrapped line end
    (dummy7)             Non-wrapped line
  info: |2
    Package dummy7 Info: genericLatexHandler unwrapping - wrap w/ parens 3
    (dummy7)             Non-wrapped line
    (dummy7)             Wrapped line start 123456789012345678901234567890123 (6789Wrapped line, continued 56789) 23456789012345678 (1234) 78901234567890123 (6789) Wrapped line end
    (dummy7)             Non-wrapped line
  name: genericLatexHandler unwrapping - wrapped line with open/close parens, test 3
  description: >-
    One of the lines is wrapped two times with many sets of open/close parens
    characters and the continuation starts at the "close parens" character.
    While a known message at the beginning of a line (in this case, the close
    parens character) should normally prevent line unwrapping, we treat this
    case specially: since there is an unmatched "open parens" character in
    the preceding line, we just unwrap the line normally.

-
  message: |2
    Package dummy8 Warning: genericLatexHandler unwrapping - wrap w/ bracket 1
    (dummy8)                Non-wrapped line
    (dummy8)                Wrapped line start 456789012345678901234567890123456789
    Wrapped line, continued 5678901234567890123456789012345678901234567890123 [6789
    ] Wrapped line end
  warning: |2
    Package dummy8 Warning: genericLatexHandler unwrapping - wrap w/ bracket 1
    (dummy8)                Non-wrapped line
    (dummy8)                Wrapped line start 456789012345678901234567890123456789Wrapped line, continued 5678901234567890123456789012345678901234567890123 [6789] Wrapped line end
  name: genericLatexHandler unwrapping - wrapped line with open/close bracket, test 1
  description: >-
    The last line is wrapped and the continuation starts at the "close bracket"
    character. While a known message at the beginning of a line (in this case,
    the close bracket character) should normally prevent line unwrapping, we
    treat this case specially: since there is an unmatched "open bracket"
    character in the preceding line, we just unwrap the line normally.

-
  message: |2
    Package dummy9 Info: genericLatexHandler unwrapping - wrap w/ bracket 2
    (dummy9)             Wrapped line start 123456789012345678901234567890123456789
    Wrapped line, continued [6789] 234567890123456789012345678901234567890123 [6789
    ] Wrapped line end
    (dummy9)             Non-wrapped line
  info: |2
    Package dummy9 Info: genericLatexHandler unwrapping - wrap w/ bracket 2
    (dummy9)             Wrapped line start 123456789012345678901234567890123456789Wrapped line, continued [6789] 234567890123456789012345678901234567890123 [6789] Wrapped line end
    (dummy9)             Non-wrapped line
  name: genericLatexHandler unwrapping - wrapped line with open/close bracket, test 2
  description: >-
    One of the lines is wrapped and the continuation starts at the "close
    bracket" character. While a known message at the beginning of a line (in
    this case, the close bracket character) should normally prevent line
    unwrapping, we treat this case specially: since there is an unmatched
    "open bracket" character in the preceding line, we just unwrap the line
    normally.

-
  message: |2
    Package dummy10 Warning: genericLatexHandler unwrapping - wrap w/ bracket 3
    (dummy10)                Wrapped line start 56789012345678901234567890123 [6789
    Wrapped line, continued 56789] 23456789012345678 [1234] 78901234567890123 [6789
    ] Wrapped line end
    (dummy10)                Non-wrapped line
  warning: |2
    Package dummy10 Warning: genericLatexHandler unwrapping - wrap w/ bracket 3
    (dummy10)                Wrapped line start 56789012345678901234567890123 [6789Wrapped line, continued 56789] 23456789012345678 [1234] 78901234567890123 [6789] Wrapped line end
    (dummy10)                Non-wrapped line
  name: genericLatexHandler unwrapping - wrapped line with open/close bracket, test 3
  description: >-
    One of the lines is wrapped two times with many sets of open/close bracket
    characters and the continuation starts at the "close bracket" character.
    While a known message at the beginning of a line (in this case, the close
    bracket character) should normally prevent line unwrapping, we treat this
    case specially: since there is an unmatched "open bracket" character in
    the preceding line, we just unwrap the line normally.
