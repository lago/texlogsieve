@default_files = ('texlogsieve.tex');

# This is simpler, but the END hack below is better IMHO
# set_tex_cmds('-halt-on-error -interaction nonstopmode %O %S | texlogsieve');
set_tex_cmds('-halt-on-error %O %S');

$pdf_mode = 4; # Use lualatex
$postscript_mode = $dvi_mode = 0;

$silent = 1; # This adds "-interaction batchmode"
$silence_logfile_warnings = 1;

# Make latexmk -c/-C clean *all* generated files
$cleanup_includes_generated = 1;
$cleanup_includes_cusdep_generated = 1;
$bibtex_use = 2;

add_cus_dep('glo', 'gls', 0, 'genHistory');
sub genHistory{
  return system("makeindex -s gglo.ist -o texlogsieve.gls texlogsieve.glo");
}

push @generated_exts, 'glo', 'gls';

# We should eat our own dog food :)
END {
  local $?; # do not override previous exit status
  if (-s "$root_filename.blg" and open my $bibfile, '<', "$root_filename.blg") {
      print("**********************\n");
      print("bibtex/biber messages:\n");
      while(my $line = <$bibfile>) {
          if ($line =~ /You.ve used/) {
              last;
          } else {
              print($line);
          };
      };
      close($bibfile);
  };

  if (-s "$root_filename.ilg" and open my $indfile, '<', "$root_filename.ilg") {
      print("*************************\n");
      print("makeindex/xindy messages:\n");
      while(my $line = <$indfile>) {
          print($line);
      };
      close($indfile);
  };

  if (-s "$root_filename.log") {
      print("***************\n");
      print("LaTeX messages:\n");
      Run_subst("texlua texlogsieve --color %R.log");
  };
};
